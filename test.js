async function a (proArr){
    let pro = proArr[0];
    let res = await pro();
    for(let i=1;i < proArr.length;i++){
        res = await proArr[i](res)
    }
    return res;
}


let proArr = [
    async function (){
        return 1
    },
    async function (){
        return 2
    },
    async function (){
        return 3
    }
]

a(proArr).then((result)=>{
    console.log(result)
})