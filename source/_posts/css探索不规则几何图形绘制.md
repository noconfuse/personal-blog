---
title: css探索不规则几何图形绘制
date: 2022-03-17 18:47:48
category: 
    - ["前端","css"]
comments: true
tags: 
    - "不规则图形绘制"
thumbnail: "https://noconfuse.gitlab.io/personal-blog/images/thumbs/1647573672729.jpg" # 一定要是远程图片
---

### clip-path


开门见山，css属性`clip-path`，这是本文的主人公。

`clip-path`：裁剪属性，在css中它可以将element元素裁剪成你想要的图形。作为一个冷门但强大的属性，clip-path可以说是一个深藏不漏，身怀绝技的武林高手。

[读者可以先自行体验](!https://bennettfeely.com/clippy/)

[一个有意思的网站](!http://www.species-in-pieces.com/#)

### 属性值含义

clip-path裁剪，其提供多种裁剪方式：inset、circle、polygon、ellipse。
```
.clip-me {

  /* 引用一个内联的 SVG <clipPath> 路径*/
  clip-path: url(#c1);

  /* 引用一个外部的 SVG  路径*/
  clip-path: url(path.svg#c1);

  /* 多边形 */
  clip-path: polygon(5% 5%, 100% 0%, 100% 75%, 75% 75%, 75% 100%, 50% 75%, 0% 75%);

  /* 圆形 */
  clip-path: circle(30px at 35px 35px);

  /* 椭圆 */
  clip-path: ellipse(65px 30px at 125px 40px);

  /* inset-rectangle() 将会替代 inset() ? */
  /* rectangle() 有可能出现于 SVG 2 */

  /* 圆角 */
  clip-path: inset(10% 10% 10% 10% round 20%, 20%);

}
```

### polygon的巧妙使用

这里重点介绍一下polygon属性值。polygon属性值是多组点的坐标，也就是说你可以在dom元素，选任意组点，用直线从第一个点一直连接到最后一个，最后自动闭合，形成的闭合图形就是裁剪后图形。

**想象一下，一笔画你可以画出多少图形？**





-----

![多个多边形](http://fdfs.xmcdn.com/group87/M05/D0/92/wKg5J17uORjDQeDoAAGwSq6BoZ830.jpeg)

![镂空遮罩](http://fdfs.xmcdn.com/group84/M05/D1/5F/wKg5JF7uORiAS7BcAAH5Ar3Oxlk65.jpeg)

上面两张图比较简陋，图中**阴影部分**即为裁剪出来的区域，是否想到clip-path的应用场景呢？


----

场景一、使用clip-path裁剪出各种几何形状的div,如果里面填充文字，就可以做出**文字环绕图片**的排版。

场景二、**镂空的遮罩效果**。

使用polygon我们还可以将其和css3动画结合在一起，做出丰富多彩的动画效果。

[示例1](!https://www.zhangxinxu.com/study/201905/css-clip-path-slide-demo.php?type=rect-sp)

[示例2](!http://www.htmleaf.com/Demo/201607023678.html)



注：微信小程序现已支持polygon属性