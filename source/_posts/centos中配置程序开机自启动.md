---
title: centos中配置程序开机自启动
date: 2022-03-30 16:08:46
category:
- ["linux基础"]
tags: "自启动程序"
thumbnail: "https://noconfuse.gitlab.io/personal-blog/images/thumbs/images.jpeg"
---

# 遇到问题

配置jar包程序自启动

搜索了解到以下几种方式

##  一、 在/ect/init.d目录下存放程序启动脚本

具体操作步骤如下，这里以activemq为例

1. `cd /ect/init.d/`
2. `touch activemq`

activemq脚本如下

```
#!/bin/sh
#
# /etc/init.d/activemq
# chkconfig: 345 63 37
# description: activemq servlet container.
# processname: activemq 5.14.1
 
# Source function library.
#. /etc/init.d/functions
# source networking configuration.
#. /etc/sysconfig/network
 
export JAVA_HOME=/usr/java/jdk1.7.0_79  #java目录，which java可以拿到
export CATALINA_HOME=/usr/home/local/services/activemq/activemq  #activemq执行脚本目录
 
case $1 in
    start)
        sh $CATALINA_HOME/bin/activemq start
    ;;
    stop)
        sh $CATALINA_HOME/bin/activemq stop
    ;;
    restart)
        sh $CATALINA_HOME/bin/activemq stop
        sleep 1
        sh $CATALINA_HOME/bin/activemq start
    ;;
 
esac
exit 0
```

3. `chmod  +x activemq ` 添加执行权限
4. `chkconfig  --add activemq` 添加到开机自启
5. `chkconfig --list` 查看自启服务中是否已经有activemq


## 二、systemd 管理服务开机自启动

1.  `cd /usr/lib/systemd/system` 进入service配置目录
2. `vim java_auto.service` 创建一个测试服务，这里以java_auto服务为例
service文件如下

```
[Unit]
Description=jeepay-manager--service
After=syslog.target network.target remote-fs.target nss-lookup.target

[Service]
Type=forking
WorkingDirectory =/home/jeepay/service/payment
ExecStart=sh app.sh start  #调用服务启动脚本
ExecStop=sh app.sh stop # 调用服务停止脚本
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

补充一下关于[service文件的说明](https://blog.csdn.net/qq_16268979/article/details/114771854)。

如果你的脚本一定要在某个目录执行添加`workingDirectory`即可

3. `systemctl daemon-reload` 重载systend配置
4. `systemctl enable java_auto` 配置java_auto服务开机自启动
5. `systemctl start java_auto` 启动服务
6. `systemctl status java_auto` 查看服务状态

{% asset_img 1648632565008.jpg jeepay_manager status %}


补充：如果遇到服务开启失败情况，可以使用`journalctl -u java_auto` 查看系统日志，如下
{% asset_img 1648632766388.jpg journalctl -u %}

可以看到 status=203，可搜索对应解决方案

## 两种方案对比

- init.d是最初的进程管理方式，且只有前一个进程完成才会启动下一个进程。
- systemd 是一种取代init.d的解决方案，推荐使用systemd
